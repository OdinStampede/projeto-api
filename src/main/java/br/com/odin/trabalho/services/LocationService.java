package br.com.odin.trabalho.services;

import br.com.odin.trabalho.models.Location;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

@Service
public class LocationService implements IpService {
    private RestClient client = RestClient.create("http://ip-api.com/");

    @Override
    public Location converteIp(String ip) {
        return client
                .get()
                .uri("/json/{ip}", ip)
                .retrieve()
                .body(Location.class);
    }
}