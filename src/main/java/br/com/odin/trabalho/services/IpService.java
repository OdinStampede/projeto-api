package br.com.odin.trabalho.services;

import br.com.odin.trabalho.models.Location;

public interface IpService {
    public Location converteIp(String ip);
}
