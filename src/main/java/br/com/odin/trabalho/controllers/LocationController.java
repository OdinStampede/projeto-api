package br.com.odin.trabalho.controllers;

import br.com.odin.trabalho.models.Location;
import br.com.odin.trabalho.services.IpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.logging.Logger;

@RestController
public class LocationController {
    @GetMapping("/ajuda")
    public ResponseEntity<String> ajuda() {
        return ResponseEntity.ok("estudante: Odin <br>, projeto: PROJETO ");
    }

    @Autowired
    private IpService ipService;
    private ArrayList<Location> listaLocation = new ArrayList<>();

    @GetMapping("/postar")
    public ResponseEntity<Object> getLocation() {
        return ResponseEntity.status(200).body(listaLocation);
    }

    @PostMapping("/capturar")
    public ResponseEntity<Object> setLocation(@RequestBody Location location) {
        System.out.print(location);
        Location locationSet = ipService.converteIp(location.getQuery());
        listaLocation.add(locationSet);
        return ResponseEntity.status(200).body(location);
    }
}