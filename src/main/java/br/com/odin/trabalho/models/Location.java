package br.com.odin.trabalho.models;

public class Location {
    private String query;
    private String city;
    private String country;

    public Location(String ip, String city, String country){
        this.query = ip;
        this.city = city;
        this.country = country;
    }
    public String getQuery(){
        return query;
    }
    public void setQuery(String query){
        this.query = query;
    }
    public String getCity(){
        return city;
    }
    public void setCity(String city){
        this.city = city;
    }
    public String getCountry(){
        return country;
    }
    public void setCountry(String country){
        this.country = country;
    }
}
